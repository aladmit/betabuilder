import pytest
import csv

from main import main

def test_main():
    main("./fixtures")

    with open('output/fixtures.csv', 'r') as f:
        r = csv.reader(f)
        table = [row for row in r]

    #         , FXMM.csv, RUSE.csv
    # FXMM.csv, 1.0,      -3.84
    # RUSE.csv, -0.02,    1.0
    assert table[1][1] == '1.0'
    assert table[1][2] == '-3.84'
    assert table[2][1] == '-0.02'
    assert table[2][2] == '1.0'
