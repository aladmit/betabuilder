import csv
import argparse
from pathlib import Path

import numpy as np

def main(data_dir):
    asset_list = sorted(Path(data_dir).glob('*.csv'))

    changes = {}

    for asset in asset_list:
        with open(asset, 'r') as f:
            changes[asset] = np.array([np.float64(row['Change %'][:-1]) for row in csv.DictReader(f)])
            print(asset, 'was imported.', str(len(changes[asset])), 'rows')

    csv_rows = []

    for asset_a in asset_list:
        row = [asset_a.name]

        for asset_b in asset_list:
            cov = np.cov(np.stack((changes[asset_a], changes[asset_b]), axis=0), bias=True)[0][1]
            var = np.var(changes[asset_b], dtype=np.float64)
            row.append(round(cov/var, 2))

        csv_rows.append(row)

    with open(f'output/{data_dir}.csv', 'w', newline='') as f:
        w = csv.writer(f, delimiter=',')
        w.writerow([''] + [a.name for a in asset_list])

        for row in csv_rows:
            w.writerow(row)

    print('Calculation was finished. Output file is', 'output/' + data_dir + '.csv')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Calculate beta`s for assets')
    parser.add_argument('data_dir', help='dir with csv files')
    args = parser.parse_args()

    main(args.data_dir)
